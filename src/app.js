// ## app.js ##
//
// This is where we set up our web application using Express. We create the
// app and set up routes which will respond to requests from the client's
// web browser.

const path = require('path');
const express = require('express');
const { table } = require('console');

// Create a new Express app
const app = express();

// Serve up our static assets from 'dist' (this includes our client-side
// bundle of JavaScript). These assets are referred to in the HTML using
// <link> and <script> tags.
app.use('/assets', express.static(path.resolve(__dirname, '..', 'dist')));

app.get('/',(req,res)=>{
   res.redirect('/index.html');
})

// Set up the index route
app.get('/index.html', (req, res) => {

  // Respond with the HTML
  let path=__dirname.substring(0,__dirname.lastIndexOf('src'));

  res.sendFile(path +"public/index.html");
});

app.get('/styles.css',(req,res)=>{
   let path=__dirname.substring(0,__dirname.lastIndexOf('src'));
   res.sendFile(path+"public/styles.css")
})


app.get('/getusers/:id', (req, res) => {
   const users = [
      {
         uid: 1,
         email: 'john@dev.com',
         personalInfo: {
            name: 'John',
            address: {
               line1: 'westwishst',
               line2: 'washmasher',
               city: 'wallas',
               state: 'WX'
            }
         }
      },
      {
         uid: 63,
         email: 'a.abken@larobe.edu.au',
         personalInfo: {
            name: 'amin',
            address: {
               line1: 'Heidelberg',
               line2: '',
               city: 'Melbourne',
               state: 'VIC'
            }
         }
      },
      {
         uid: 45,
         email: 'Linda.Paterson@gmail.com',
         personalInfo: {
            name: 'Linda',
            address: {
               line1: 'Cherry st',
               line2: 'Kangaroo Point',
               city: 'Brisbane',
               state: 'QLD'
            }
         }
      }
   ];

  let isSort=false;

   if(req.params.id=='1')//no sorting
   {
      isSort = true;
   }

   var refinedUsers = returnUsers(users,isSort);

   var tablehtml = '<link rel="stylesheet" href="styles.css"><table><thead><tr><td>Name</td><td>Email</td><td>State</td></tr></thead>' +
      '<tbody>';
   for (let i = 0; i < refinedUsers.length; i++) {
      var row = '<tr><td>' + refinedUsers[i]['Name'] + '</td><td>' + refinedUsers[i]['Email'] + '</td><td>' + refinedUsers[i]['State'] + '</td></tr>';
      tablehtml += row;
   }

   tablehtml += '</tbody></table>';

   res.send(tablehtml);

});

function returnUsers(users,doSort) {
   var outArr = [];
   for (var i = 0; i < users.length; i++) {
      outArr.push(Object.assign({}, { Name: users[i]['personalInfo']['name'], Email: users[i]['email'], State: users[i]['personalInfo']['address']['state'] }))
   }
   if(doSort){
      outArr= outArr.sort(function(a, b) {
         var nameA = a.Name.toUpperCase(); // ignore upper and lowercase
         var nameB = b.Name.toUpperCase(); // ignore upper and lowercase
         if (nameA < nameB) {
           return -1;
         }
         if (nameA > nameB) {
           return 1;
         }
       
         // names must be equal
         return 0;
      });
   }
   return outArr;
}

// Export the Express app
module.exports = app;
